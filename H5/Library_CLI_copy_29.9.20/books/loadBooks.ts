//Load books from JSON and return an array
import fs from "fs";    //file system
import IBook from "../interfaces/IBook";



const loadBooks = () => {
    // try{
    const books: IBook[] = JSON.parse(fs.readFileSync("./books/books.json", "utf-8"));
    return books;
    // }catch(err){
    //     console.log(err.message);

    // };
};



export default loadBooks;
