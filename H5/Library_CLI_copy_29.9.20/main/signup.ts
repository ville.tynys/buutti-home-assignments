import readline from "readline-sync";
import IUser from '../interfaces/IUser';
import loadUsers from '../users/loadUsers';
import saveUsers from '../users/saveUsers';

function signupCommand() {
   let users = loadUsers();
   console.log("Creating a new user account.");
   const signUpName: string = readline.question("Insert your name: ");
   function enterPassword() {
      const signUpPassword: string = readline.question("Insert new password: ");
      const passwordAgain: string = readline.question("Re-enter your password to ensure it matches the one given above: ");
      if (signUpPassword === passwordAgain) {
         console.log("Passwords match.");
         let accountId: string = Math.floor(Math.random() * (90000000 - 10000000) + 10000000).toString();
         while (users.find((user: IUser) => user.id === accountId)) {
            accountId = Math.floor(Math.random() * (90000000 - 10000000) + 10000000).toString();
         }
         let newUser: IUser = {
            name: signUpName,
            password: signUpPassword,
            id: accountId,
            books: []
         }
         users.push(newUser);
         saveUsers(users);
         console.log("Your account is now created.");
         console.log(`Your account id is ${accountId}.`);
         console.log("Store your account ID in a safe place, preferably in a password manager.");
         console.log("Use the command 'login' to log in to your account.");
      } else {
         console.log("Passwords do not match.");
         enterPassword();
      }
   }
   enterPassword();
}

export default signupCommand;