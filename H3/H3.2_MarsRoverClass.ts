class Rover {

    //parameters
    position: number[];     //x and y
    directions: string[] = ["NORTH", "EAST", "SOUTH", "WEST"];
    direction_i: number;
    log: any = [];          //TODO change any to something other..

    //constructor
    constructor(position: number[] = [5, 0], direction_i = 2) {
        this.position = position;
        this.direction_i = direction_i; //suunnan state
        this.log.push(this.position)
    }



    //class methods
    // y -akseli tehtävänannossa erikoinen: esim. jos mennään south, y kasvaa?      //PERTTU: Tämä on yleinen käytäntö. Ehkä alkujaan siitä et näytön pikselit indeksöidään niin et origo lähtee vas. yläreunasta...
    moveForward() {
        if (this.direction_i % 4 === 0) { //jos north, y pienenee
            this.position[1]--
            this.log.push(this.position)                                            //PERTTU: if else , else tyylinen kontrollirakenne parempi (ei käy kaikkia turhaan läpi jne.)
        };
        if (this.direction_i % 4 === 1) { //jos east, x kasvaa
            this.position[0]++
            this.log.push(this.position)
        };
        if (this.direction_i % 4 === 2) { //jos south, y kasvaa
            this.position[1]++
            this.log.push(this.position)
        };
        if (this.direction_i % 4 === 3) { //jos west, x pienenee
            this.position[0]--
            this.log.push(this.position)
        };

        console.log("Moving towards ", this.directions[this.direction_i % 4], "to coordinates ", this.position)

    };

    turnLeft() {
        this.direction_i--;
        console.log("Turning left to face", this.directions[this.direction_i % 4],)
    };

    turnRight() {
        this.direction_i++;
        console.log("Turning right to face", this.directions[this.direction_i % 4],)
    };

    printPosition() {
        console.log("Currently positioned in", this.position)
    };

    printLog() {
        console.log("Travel Log: \n ", this.log);
    };

};

export default Rover;


//Giving commands
let marsRover = new Rover();


marsRover.moveForward();
marsRover.turnRight();
marsRover.moveForward();
marsRover.turnLeft();
marsRover.moveForward();
marsRover.printPosition();
marsRover.printLog();
