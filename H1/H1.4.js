// ### H1.4
// Write a program that prompts the user Input number calculates their sum.

// For example:
// ```
// node sum.js
// Input number1: 1
// Input number2: 2
// Input number3: 3
// The sum is 6.
// ```

const readline = require("readline-sync");

let number_1 = Number(readline.question("Input number 1: "));     //to make better we could test that user really inputs a number
let number_2 = Number(readline.question("Input number 2: "));
let number_3 = Number(readline.question("Input number 3: "));

console.log('The sum is', number_1 + number_2 + number_3);

