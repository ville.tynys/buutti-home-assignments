# Note-taking application

**Deadline 9.10.2020 10am**

Please save your home assignment as `H6_Firstname_Lastname.zip` and send it to the instructor as a DM in Discord OR if you'd like to, you can create a GitLab repository for your home assignments and send me a link to it.

Remember to include `tsconfig.json` and `package.json` files for easy installation. Especially now that it's a React app!

Create a simple note-taking app in React.ts. So it's a CRUD (Create, read, update, delete) exercise! 

It should have at least two React components, the root component `App` and `Note` which is used for rendering all the notes.

### H6.1 Create

Create a form for adding new notes. It should have at least a text field and a button for adding the note.

### H6.2 Read

The added notes should go to an array whose elements get rendered for the user. You can use the `map` array method for rendering every note at once!

### H6.3 Delete

The notes should have a delete button which, well, deletes the note from the array.

## Extra

### H6.4 Update

The already-added notes should have an edit button that when clicked, opens a new text input field where the contents of the note can be updated.

Add the previous contents of the note to the input field for convenience!

**Hint:** You can use conditional rendering to hide/show elements like the input field when needed.

### H6.5 CSS

Add styling for the notes and the new note form with CSS.