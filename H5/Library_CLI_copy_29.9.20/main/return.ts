import loadUsers from '../users/loadUsers';
import saveUsers from '../users/saveUsers';
import state from '../state';
import readline from 'readline-sync';
import loadBooks from '../books/loadBooks';
import saveBooks from '../books/saveBooks';

function returnCommand() {
   const books = loadBooks();
   const users = loadUsers();
   const userName = state.getUser();
   const ownAccount = users.find((user) => user.name === userName);
   const borrowedBooks = ownAccount?.books;
   if (borrowedBooks && borrowedBooks.length === 0) {
      console.log(`You don't have borrowed books.`);
      return;
   }
   if (borrowedBooks && borrowedBooks.length > 0) {
      console.log('');
      console.log('List of your borrowed books: ');
      console.log(borrowedBooks);
      const ISBN = readline.question(
         'Give ISBN of the book you want to return: '
      );
      const book = borrowedBooks.find((book) => book.isbn === ISBN);
      const id = book?.id;
      const updatedBookList = ownAccount?.books.filter(
         (book) => book.isbn !== ISBN
      );
      if (ownAccount && updatedBookList) {
         ownAccount.books = updatedBookList;
         const filteredUserList = users.filter(
            (user) => user.name !== ownAccount.name
         );
         // updating users json
         filteredUserList.push(ownAccount);
         saveUsers(filteredUserList);

         // updating status of the book in the book json
         const bookCopies = books.find((book) => book.isbn === ISBN);
         if (bookCopies) {
            for (let i = 0; i < bookCopies.copies.length; i++) {
               if (bookCopies.copies[i].id === id) {
                  bookCopies.copies[i].borrower_id = null;
                  bookCopies.copies[i].due_date = null;
                  bookCopies.copies[i].status = 'in_library';
                  break;
               }
            }
            const filteredBookList = books.filter((book) => book.isbn !== ISBN);
            filteredBookList.push(bookCopies);
            saveBooks(filteredBookList);
         }
         console.log(`You have returned book which ISBN is ${ISBN}.`);
      }
   }
}

export default returnCommand;
