import state from "../state";
import loadUsers from "../users/loadUsers";
import logoutCommand from "./logout";
import loadBooks from "../books/loadBooks";
import saveUsers from "../users/saveUsers";

function removeAccountCommand() {
   const users = loadUsers();
   const books = loadBooks();
   const userName = state.getUser();
   const ownAccount = users.find((user) => user.name === userName);
   const borrowedBooks = ownAccount?.books;
   
   //check if user has books borrowed
   if (borrowedBooks && borrowedBooks.length > 0) {
      console.log(`Please return the following borrowed books before removing the account:`);
      console.log(borrowedBooks);
      console.log('');
      return;
   }


   //load users, remove user from the users and save.
   const new_users = users.filter(user => user.id !== ownAccount?.id);
   saveUsers(new_users);

   //logout user (to clear user from state)
   logoutCommand();
   console.log("Account removed successfully");
    
}

export default removeAccountCommand;