import fs from 'fs';
import state from '../state';

function logoutCommand() {
  try {
    state.setLoginFalse();
    console.log('You logged out.');
  } catch (e) {
    console.log(e);
  }
}

export default logoutCommand;
