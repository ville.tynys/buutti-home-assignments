const readline = require("readline-sync");

const number_1 = Number(readline.question("Give me a number plz: "));
const number_2 = Number(readline.question("Give me a number plz: "));
const number_3 = Number(readline.question("Give me a number plz: "));
let smallest;
let biggest;

//find smallest
// if(number_1<number_2 && number_1<number_3){
//     smallest=number_1;
// } else{
//     if(number_2<number_3){
//         smallest=number_2
//     } else{
//         smallest=number_3
//     };
// };

smallest = Math.min(number_1, number_2, number_3);


//find biggest
// if(number_1>number_2 && number_1>number_3){
//     biggest=number_1;
// } else{
//     if(number_2>number_3){
//         biggest=number_2
//     } else{
//         biggest=number_3
//     };
// };

biggest = Math.max(number_1, number_2, number_3);

console.log('Smallest is: ', smallest);
console.log('Biggest is: ', biggest);