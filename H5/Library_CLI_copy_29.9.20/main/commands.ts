import helpCommand from './help';
import quitCommand from './quit';
import searchCommand from './search';
import signupCommand from './signup';
import loginCommand from './login';
import listCommand from './list';
import borrowCommand from './borrow';
import returnCommand from './return';
import changeNameCommand from './change_name';
import removeAccountCommand from './remove_account';
import logoutCommand from './logout';
import Command from '../interfaces/ICommand';

const commands: Command[] = [
  {
    keyWord: 'help',
    status: 'always',
    commandFunction: helpCommand,
  },
  {
    keyWord: 'quit',
    status: 'always',
    commandFunction: quitCommand,
  },
  {
    keyWord: 'search',
    status: 'always',
    commandFunction: searchCommand,
  },
  {
    keyWord: 'signup',
    status: 'logged_out',
    commandFunction: signupCommand,
  },
  {
    keyWord: 'login',
    status: 'logged_out',
    commandFunction: loginCommand,
  },
  {
    keyWord: 'list',
    status: 'logged_in',
    commandFunction: listCommand,
  },
  {
    keyWord: 'borrow',
    status: 'logged_in',
    commandFunction: borrowCommand,
  },
  {
    keyWord: 'return',
    status: 'logged_in',
    commandFunction: returnCommand,
  },
  {
    keyWord: 'change_name',
    status: 'logged_in',
    commandFunction: changeNameCommand,
  },
  {
    keyWord: 'remove_account',
    status: 'logged_in',
    commandFunction: removeAccountCommand,
  },
  {
    keyWord: 'logout',
    status: 'logged_in',
    commandFunction: logoutCommand,
  },
];

export default commands;
