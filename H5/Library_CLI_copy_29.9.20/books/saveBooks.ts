//Save books from array to JSON
import fs from "fs";    //file system
import IBook from "../interfaces/IBook";



const saveBooks = (books: IBook[]) => {
   try {
      let books_String = JSON.stringify(books);
      fs.writeFileSync("./books/books.json", books_String, "utf8");
   } catch (err) {
      console.log(err.message);
   };
};

export default saveBooks;
