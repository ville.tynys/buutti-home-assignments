export default interface IBorrowed {
   isbn: string;
   title: string;
   id: string;
   dueDate: string | null;
}