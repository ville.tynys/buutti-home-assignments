import readline from 'readline-sync';
import commandHandler from './main/commandHandler';

console.log('Welcome to Varpaisjärvi library!');
console.log("Get the list of available commands by typing 'help'.");

while (true) {
  let userInput = readline.question('Give a command: ');
  commandHandler(userInput);
}
