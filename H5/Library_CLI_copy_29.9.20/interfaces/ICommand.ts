export default interface Command {
  keyWord: string;
  status: string;
  commandFunction: () => void;
}
