import IUser from "../interfaces/IUser";
import fs from "fs";


const loadUsers = () => {
    // try{
    const users: IUser[] = JSON.parse(fs.readFileSync("./users/users.json", "utf-8"));
    return users;
    // }catch(err){
    //     console.log(err.message);

    // };
};

export default loadUsers;