import commands from './commands';
import state from '../state';

const commandHandler = (input: string) => {
  const command = commands.find((command) => command.keyWord === input);
  const isLogin = state.getLoginStatus();
  if (command) {
    const status = command.status;
    if (status === 'always') {
      command.commandFunction();
    }
    if (status === 'logged_in' && isLogin) {
      command.commandFunction();
    }
    if (status === 'logged_out' && !isLogin) {
      command.commandFunction();
    }
  } else {
    console.log("Command didn't found.");
    console.log("Get the list of available commands by typing 'help'.");
  }
};

export default commandHandler;
