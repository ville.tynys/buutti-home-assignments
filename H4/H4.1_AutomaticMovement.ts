import Rover from "./MarsRover_class";

const grid_boundaries: any =
    { west: 0, east: 10, north: 0, south: 10 }
    
const marsRover = new Rover(grid_boundaries);




for (let i = 0; i < 12; i++) {
    marsRover.moveForward();
}


marsRover.turnRight();
for (let i = 0; i < 12; i++) {
    marsRover.moveForward();
}

marsRover.moveForward();
marsRover.turnLeft();
marsRover.moveForward();
marsRover.printPosition();
marsRover.printLog();


