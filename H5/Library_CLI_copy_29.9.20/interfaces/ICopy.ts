export default interface iCopy {
   id: string;
   status: string;
   due_date: string | null;
   borrower_id: string | null;
}
