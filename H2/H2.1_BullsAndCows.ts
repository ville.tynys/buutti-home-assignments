
// ## H2.1 Bulls and Cows
// Bulls and Cows is a game for two players where the first one creates a secret word and the second one tries to guess it.
//After every guess, the first player tells whether the secret word and the guessed word have matching characters in right positions ("bulls") or wrong positions ("cows").

// For example, if the secret word is `HEAT`, a guess of
//  - `COIN` would result in `0 Bulls, 0 Cows` (all of the guessed letters are wrong),
//  - `EATS` would result in `0 Bulls, 3 Cows` (since E, A, T are in the wrong positions),
//  - `TEAL` would result in `2 Bulls, 1 Cow` (since E and A are in the right positions and T is in the wrong position) and
//  - `HATE` would result in `1 Bull, 3 Cows`.

// The game would continue until the second player scores `4 Bulls` for guessing `HEAT`.
// Your task is to create this game as a command line program where the computer is the first player. 




import readline from 'readline-sync';

const secret_word: string = "FREE"      //this could be made random generated..
let guessed_word: string;
let bulls: number = 0;
let cows: number = 0;
let num_of_rounds_played: number = 0;
console.log('Secret word selected! \n\r      ***  ')


//GAME LOGIC
//loop until Player2 guesses the word right.
do {
    bulls = 0;
    cows = 0;
    num_of_rounds_played++;

    //array from user guess
    do {
        guessed_word = readline.question(`The secret word is ${secret_word.length} letters long. Try to guess it: `).toUpperCase();    //uppercase makes comparison not case insensitive
        if (guessed_word.length > secret_word.length) {
            console.log('your guess was too long!!!!!')
        };
    } while (guessed_word.length > secret_word.length);

    //compare
    for (let i = 0; i < secret_word.length; i++) {
        //if is bull
        if (guessed_word[i] === secret_word[i]) {
            bulls++

            //else test if cow
        } else {
            for (let j = 0; j < secret_word.length; j++) {
                if (guessed_word[i] === secret_word[j]) {
                    cows++
                }
            }
        }
    }
    console.log(` \n\r      ***  \n\rBulls: ${bulls}, Cows: ${cows}.`);
} while (bulls < secret_word.length);

console.log(`Congratulations! Number of rounds played: ${num_of_rounds_played}`);

