/**
 * Input: a book object
 * Outputs: book details into console
 */

import IBook from "../interfaces/IBook";

const printBookDetails = (book: IBook) => {
   console.log(`${book.title} by ${book.author} (${book.published.slice(0, 4)})`);
   console.log(`Books in library: ${book.copies.length}`);
   let availableBooks = book.copies.filter((book: any) => book.status === "in_library");
   console.log(`Available for borrowing: ${availableBooks.length}`);
}

export default printBookDetails;