import { title } from 'process';
import loadBooks from './loadBooks';

export const getBookByISBN = (isbn: string) => {
  const books = loadBooks();
  const book = books.find((book) => book.isbn === isbn);
  return book;
};

export const getBookByAuthor = (author: string, title: string) => {
  const books = loadBooks();
  const book = books.find(
    (book) =>
      book.author.toLowerCase() === author.toLowerCase() &&
      book.title.toLowerCase() === title.toLowerCase()
  );
  return book;
};
