import readline from 'readline-sync';
import printBookDetails from '../main/printBookDetails';
import { getBookByAuthor } from '../books/getBook';
import { getBookByISBN } from '../books/getBook';

function searchCommand() {
  console.log('');
  console.log('You can search books by commands which are listed below: ');
  console.log('');
  console.log('isbn ------ search book by isbn number');
  console.log("author ---- search book by author's name");
  console.log('menu ------ return to menu');
  console.log('');

  let searchStatus = true;
  while (searchStatus) {
    let userInput = readline.question('Give a search command: ');
    if (
      userInput === 'isbn' ||
      userInput === 'author' ||
      userInput === 'menu'
    ) {
      console.log('');
      if (userInput === 'menu') {
        console.log('');
        console.log('Returning to menu.');
        console.log('');
        searchStatus = false;
      }
      if (userInput === 'author') {
        const authorName = readline.question('Give an author: ');
        const bookTitle = readline.question('Give a title of the book: ');
        const book = getBookByAuthor(authorName, bookTitle);
        if (book) {
          console.log('');
          printBookDetails(book);
        } else {
          console.log('');
          console.log('Books not found.');
        }
      }
      if (userInput === 'isbn') {
        const isbn = readline.question('Give an ISBN of book: ');
        const book = getBookByISBN(isbn);
        if (book) {
          console.log('');
          printBookDetails(book);
        } else {
          console.log('');
          console.log('Books not found.');
        }
      }
    } else {
      console.log('');
      console.log(
        'Command not found. You can search books by commands which are listed below:  '
      );
      console.log('');
      console.log('isbn ------ search book by isbn number');
      console.log("author ---- search book by author's name");
      console.log('menu ------ return to menu');
      console.log('');
    }
  }
}

export default searchCommand;
