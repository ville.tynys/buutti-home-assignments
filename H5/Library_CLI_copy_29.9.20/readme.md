# Library CLI application.

This assignment is the first group project of the Buutti TypeScript course. Later, based on this, we will create a full stack application that has a web UI and a backend server. In this CLI version, "frontend" is just the command line dialogue, and "backend" has a database of books and users.

## Technologies

TypeScript/JavaScript, node.js, JSON

## Installation and Usage
```bash
git clone
cd group-project-purple
npm install
npx ts-node index.js
```

After this the program should run and ask for commands. You can type "help" to see all commands.

## Contributing
This project is for study purposes only.

## Team
@ville.tynys
@samjok
@joonavainiola

## License
[MIT](https://choosealicense.com/licenses/mit/)