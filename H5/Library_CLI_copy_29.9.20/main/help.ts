function helpCommand() {
   console.log(
      "Here's a list of commands you can use!\n\nAVAILABLE EVERYWHERE\nhelp\t\tPrints the listing.\nquit\t\tQuits the program.\nsearch\t\tOpens a dialog for searching for a book in the library system.\n\nONLY AVAILABLE WHEN LOGGED OUT\nsignup\t\tOpens a dialog for creating a new account.\nlogin\t\tOpens a dialog fot loggint into an existing account.\n\nONLY AVAILABLE WHEN LOGGED IN\nlist\t\tList the books you are currently borrowing.\nborrow\t\tOpens a dialog for borrowing a book.\nreturn\t\tOpens a dialog for returning a book.\nchange_name\tOpens a dialog for changing the name associated with the account.\nremove_account\tOpens a dialog for removing the currently logged in account from the library system.\nlogout\t\tLogs out the currently logged in user."
   );
}

export default helpCommand; 