import IUser from '../interfaces/IUser';
import fs from 'fs';

const saveUsers = (users: IUser[]): void => {
  try {
    fs.writeFileSync('./users/users.json', JSON.stringify(users));
  } catch (err) {
    console.log(err.message);
  }
};

export default saveUsers;
