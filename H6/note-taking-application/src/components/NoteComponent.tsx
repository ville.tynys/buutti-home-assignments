import React, { useState } from "react";

interface INoteComponent {
	key: string;
	id:string;
	note: unknown;
	noteText: string;
	deleteNote: ((id:string)=> void);
	editNote: ((id: string, textState: string)=> void);
}

const NoteComponent: React.FC<INoteComponent> = ({id, noteText, editNote, deleteNote}:INoteComponent) => {
	// console.log("note component: ",
	const [isShowing, setIsShowing] = useState(false);
	const [textState, setTextState] = useState(noteText);

	const toggle = () => {
		setIsShowing(!isShowing);
	};

	return (
		<div className="notecomponent-div">
			{isShowing ?
				(<div className="notebox-edit">
					<input className="notebox-input"
						value={textState}
						onChange={(event) =>
							setTextState(event.target.value)}></input>
					<button onClick={() => { editNote(id, textState); toggle(); }}>Save</button>
				</div>)
				:

				(<div className="notebox-basic">
					<p>{noteText}</p>
					<button onClick={() => {
						toggle();
					}}>Edit</button>
					<button onClick={() => { deleteNote(id); }}>Delete</button>
				</div>)}
		</div>
	);
};


export default NoteComponent;