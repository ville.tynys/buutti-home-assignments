import express from "express";
import bodyParser from "body-parser";
import loadUsers from "./users/loadUsers";
import IUser from "./interfaces/IUser";
import saveUsers from "./users/saveUsers";
import loadBooks from "./books/loadBooks";
import state from "./state";
import IBorrowed from "./interfaces/IBorrowed";
import saveBooks from "./books/saveBooks";
import borrowCommand from "./main/borrow";

const app = express();
const PORT = 5001;



//MIDDLEWARES
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
    console.log(`METHOD: ${req.method}`);
    console.log(`PATH: ${req.path}`);
    console.log("BODY: ", req.body);
    console.log(`---`);
    next();
});


//H5.1 Create a new user with POST 
app.post("/library/new_user/:name/:password", (req, res, next) => {
    let users = loadUsers();
    console.log("Creating a new user account.");
    const signUpName: string = req.params.name;
    const signUpPassword: string = req.params.password;

    let accountId: string = Math.floor(Math.random() * (90000000 - 10000000) + 10000000).toString();
    while (users.find((user: IUser) => user.id === accountId)) {
        accountId = Math.floor(Math.random() * (90000000 - 10000000) + 10000000).toString();
    }
    let newUser: IUser = {
        name: signUpName,
        password: signUpPassword,
        id: accountId,
        books: []
    }
    users.push(newUser);
    saveUsers(users);
    res.send(accountId)
});


//H5.2 GET number of borrowed books
app.get("/library/:user_id/books_n", (req, res) => {
    // console.log(req.params.user_id);
    const users = loadUsers();
    const ownAccount = users.find((user) => user.id === req.params.user_id);            //use id to check which books are borrowed and send result to the client as a string.
    const borrowedBooks = ownAccount?.books;
    // console.log(borrowedBooks?.length.toString());
    res.send(borrowedBooks?.length.toString());
});

//H5.3 GET a list of borrowed books
app.get("/library/:user_id/books", (req, res) => {
    // console.log(req.params.user_id);
    const users = loadUsers();
    const ownAccount2 = users.find(user => user.id === req.params.user_id);
    const borrowedBooks2 = ownAccount2?.books;
    // console.log(borrowedBooks2);
    res.send(JSON.stringify(borrowedBooks2));
});

//H5.4 Borrow a book with PATCH
app.patch("/library/user/borrow/:id/:password/:isbn", (req, res) => {
    const users = loadUsers();
    const books = loadBooks();
    let idParam = req.params.id
    let passwordParam = req.params.password;
    let isbnParam = req.params.isbn
    const newUser = users.find((user: IUser) => user.password === passwordParam && user.id === idParam);

    if (newUser) {
        console.log("user has been recognized as:   ", newUser);

        //checks if book with chosen isbn is in library, 
        //runs through copies and borrows the first available copy for the user
        const book = books.find(({ isbn }: any) => isbn === isbnParam);
        if (book) {
            let availableBooks = book.copies.filter((book) => book.status === "in_library");

            if (availableBooks.length > 0) {
                for (let i = 0; i < book.copies.length; i++) {
                    if (book.copies[i].borrower_id === null) {                         //borrowing the book for the user
                        book.copies[i].status = "borrowed";
                        book.copies[i].borrower_id = newUser.id;
                        book.copies[i].due_date = new Date(Date.now() + 12096e5).toISOString();
                        const bookDetails: IBorrowed = {
                            isbn: book.isbn,
                            title: book.title,
                            id: book.copies[i].id,
                            dueDate: book.copies[i].due_date
                        }
                        newUser.books.push(bookDetails);
                        res.send(bookDetails);
                        saveUsers(users);
                        saveBooks(books);
                        console.log("Borrowed!\n");
                    }
                }
            } else {
                res.send("This book is not available to be borrowed.\n");

            }
        } else {
            res.send("No books found for that ISBN! Try again.\n");
        }
    } else {
        res.send("user was not identified")
    }
});



//TODO RETURN A BOOK WITH PATCH
app.patch("/library/user/return/:id/:password/:isbn", (req, res) => {
    const books = loadBooks();
    const users = loadUsers();
    const ownAccount = users.find((user) => user.id === req.params.id);
    const borrowedBooks = ownAccount?.books;
    if (borrowedBooks && borrowedBooks.length === 0) {
        console.log(`No borrowed books.`);
        return res.status(200).send("there were no borrowed books under this user.")
    }



    if (borrowedBooks && borrowedBooks.length > 0) {     //jos käyttäjällä on lainattuja kirjoa
        console.log(borrowedBooks);
        const ISBN = req.params.isbn;
        const book = borrowedBooks.find((book) => book.isbn === ISBN); //book on täsä palautettava kirja
        const id = book?.id;
        const updatedBookList = ownAccount?.books.filter(
            (book) => book.isbn !== ISBN
        );
        if (ownAccount && updatedBookList) {
            ownAccount.books = updatedBookList;             
            const filteredUserList = users.filter((user) => user.name !== ownAccount.name);     //päivitetään käyttäjän kirjat (poistetaan vanha ja uus tilalle pushilla)   
            filteredUserList.push(ownAccount);
            saveUsers(filteredUserList);

            
            const bookCopies = books.find((book) => book.isbn === ISBN);            // removing "borrowed" status of the book in the book json
            if (bookCopies) {
                for (let i = 0; i < bookCopies.copies.length; i++) {
                    if (bookCopies.copies[i].id === id) {
                        bookCopies.copies[i].borrower_id = null;
                        bookCopies.copies[i].due_date = null;
                        bookCopies.copies[i].status = "in_library";
                        break;
                    }
                }
                const filteredBookList = books.filter((book) => book.isbn !== ISBN);    //päivitetään  books.json (poistetaan vanha ja uus tilalle pushilla)   
                filteredBookList.push(bookCopies);
                saveBooks(filteredBookList);
            }
            res.send(book);
        }
    }

});






app.listen(PORT);
console.log("listening to port: " + PORT);
