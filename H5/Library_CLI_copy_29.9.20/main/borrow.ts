import readline from "readline-sync";
import state from "../state";
import loadUsers from '../users/loadUsers';
import saveUsers from '../users/saveUsers';
import loadBooks from '../books/loadBooks';
import saveBooks from '../books/saveBooks';
import IBorrowed from "../interfaces/IBorrowed";
import printBookDetails from "./printBookDetails";

function borrowCommand() {
   let books = loadBooks();
   let users = loadUsers();
   const isbnQuery: string = readline.question("Give ISBN of the book to borrow (or type 'quit' to quit the dialog): ");
   if (isbnQuery === "quit") {
      return;
   }

   const book = books.find(({ isbn }: any) => isbn === isbnQuery);
   if (book) {
      console.log("\nFound book:");
      printBookDetails(book);
      console.log("");
      let availableBooks = book.copies.filter((book) => book.status === "in_library");
      if (availableBooks.length > 0) {
         let answer = readline.question(`Borrow ${book.title} by ${book.author} (${book.published.slice(0, 4)})? (y/n) `);
         if (answer.toUpperCase() === "Y") {
            for (let i = 0; i < book.copies.length; i++) {
               if (book.copies[i].borrower_id === null) {
                  const user = users.find(({ name }: any) => name === state.getUser());
                  if (user) {
                     book.copies[i].status = "borrowed";
                     book.copies[i].borrower_id = user.id;
                     book.copies[i].due_date = new Date(Date.now() + 12096e5).toISOString();
                     const bookDetails: IBorrowed = {
                        isbn: book.isbn,
                        title: book.title,
                        id: book.copies[i].id,
                        dueDate: book.copies[i].due_date
                     }
                     user.books.push(bookDetails);
                  }
                  saveUsers(users);
                  saveBooks(books);
                  console.log("Borrowed!\n");
                  break;
               }
            }
         } else if (answer.toUpperCase() === "N") {
            borrowCommand();
         } else {
            console.log("Not 'y' or 'n'. Try again.\n");
            borrowCommand();
         }
      } else {
         console.log("This book is not available to be borrowed.\n");
         borrowCommand();
      }

   } else {
      console.log("No books found for that ISBN! Try again.\n");
      borrowCommand();
   }
}

export default borrowCommand;