import React, { useState } from "react";
import notesData from "./notesData";
import NoteComponent from "./NoteComponent";


const Note: React.FC = () => {
	const [note, setNote] = useState("");
	const [notes, setNotes] = useState(notesData);

	const saveNote = (newNote: string) => {
		setNotes([...notes, { id: String((notes.length + 1)), noteText: newNote }]);   //adds new object to notes array with a new unique string
	};

	const deleteNote = (id: string) => {
		setNotes(notes.filter(note => note.id !== id)); //Filtering is ok because it returns a new array..
	};

	const editNote = (id: string, textState: string) => {
		const elementsIndex = notes.findIndex(element => element.id === id);
		const newNotes = [...notes];
		newNotes[elementsIndex] = { ...newNotes[elementsIndex], noteText: textState };
		setNotes(newNotes);
	};



	return (
		<div className="note-container-div">
			<div className="savenote-div">
				<p>Write a note and click &quot;save&quot;</p>
				<input onChange={(event) => setNote(event.target.value)}></input>
				<button onClick={() => saveNote(note)}>Save</button>
			</div>
			<div className="notecomponent-container-div">
				{
					notes.map((note) =>
						(<NoteComponent key={note.id} id={note.id} note={note} noteText={note.noteText} deleteNote={deleteNote} editNote={editNote} />))
				}
			</div>

		</div>

	);
};

export default Note;