import Rover from "./H3.2_MarsRoverClass";

// ### H4.3 Two rovers
// Create another instance of the Rover class.
//Give the rover a name and the starting position as an argument of the constructor function of the class.


class AnotherRover extends Rover {
    name: string;

    public constructor(name: string = "some name", position: number[] = [1, 1]) {
        super(position);
        this.name = name;
    };
};


let rauli = new AnotherRover("Ropotti Rauli", [1, 1]);
console.log(rauli.name, rauli.position);


