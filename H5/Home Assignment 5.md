# API for the Library

**Deadline 2.10.2020 10am**

Please save your home assignment as `H5_Firstname_Lastname.zip` and send it to the instructor as a DM in Discord OR if you'd like to, you can create a GitLab repository for your home assignments and send me a link to it.

Remember to include `tsconfig.json` and `package.json` files for easy installation.

In this assignment, we aim to create a rudimentary API for our previous group exercise CLI application - one that can be used to receive requests and send appropriate responses.

Note that the API of this exercise does not have to be connected to your group exercise, if you want to keep things simple. Also, all the sub-exercises of this home assignment can reside inside one single `.ts` file.

You can either use an HTML page that has a form in it, VS Code REST client extension, or Postman, to test the interface.

This exercise consists of multiple request specifications, introduced in the following format:

### H5.X Request description
1. parameters the user should send to the API
2. path for endpoint
3. value the banking API should send back to user

Include Express in the application and create the following endpoints for your API.

### H5.1 Create a new user with POST
1. name, password
2. /library/new_user
3. user_id

### H5.2 GET number of borrowed books
1. id
2. /library/:user_id/books_n
3. number_of_borrowed_books

### H5.3 GET a list of borrowed books
1. id
2. /library/:user_id/books
3. borrowed_books

### H5.4 Borrow a book with PATCH
1. id, password, isbn
2. /library/user/borrow
3. borrowed_book (as an object)

This should result in an error if the book is not available for borrowing!

### H5.5 Return a book with PATCH
1. id, password, isbn
2. /library/user/return
3. returned_book (as an object)

## Extra:

### H5.6 Update account with PATCH

Update username:

1. id, new_name
2. /library/user/name
3. new_username

Update password:

1. id, new_password
2. /library/user/password
3. new_password