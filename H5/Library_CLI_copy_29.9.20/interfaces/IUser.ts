import IBorrowed from './IBorrowed';

export default interface IUser {
   name: string;
   password: string;
   id: string;
   books: IBorrowed[];
}