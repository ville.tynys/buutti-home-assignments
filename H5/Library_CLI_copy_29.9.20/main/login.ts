import readline from 'readline-sync';
import state from '../state';
import IUser from '../interfaces/IUser';
import loadUsers from '../users/loadUsers';

let calculator = 0;

function loginCommand() {
  try {
    const users = loadUsers();
    let password = readline.question('Type your password: ');
    let id = readline.question('Type your account id: ');
    const newUser = users.find(
      (user: IUser) => user.password === password && user.id === id
    );

    if (newUser) {
      state.setLogin(newUser.name);
      console.log(`User ${state.getUser()} has logged in.`);
    } else {
      calculator++;
      console.log('You gave wrong ID or password. Try again.');
      if (calculator < 3) {
        loginCommand();
      } else {
        console.log('You tried too many times and returned to menu.');
        console.log('');
      }
    }
  } catch (e) {
    console.log(e);
  }
}

export default loginCommand;
