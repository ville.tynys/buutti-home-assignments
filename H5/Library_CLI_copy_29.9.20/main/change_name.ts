function changeNameCommand() {
  console.log(
    "This command is not available at this program version. Type 'help' if you want to see all commands."
  );
}

export default changeNameCommand;
