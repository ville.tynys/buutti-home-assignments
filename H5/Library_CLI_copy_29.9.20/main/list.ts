import state from "../state";
import loadUsers from '../users/loadUsers';
import loadBooks from '../books/loadBooks';

function listCommand() {
   const users = loadUsers();
   const user = users.find(({ name }: any) => name === state.getUser());
   const books = loadBooks();
   if (user) {
      if (user.books.length > 0) {
         console.log("\nBooks you've borrowed:\n");
         for (let i = 0; i < user.books.length; i++) {
            let book = books.find(({ isbn }: any) => isbn === user.books[i].isbn);
            if (book) {
               console.log(`${book.title} by ${book.author} (${book.published.slice(0, 4)})`);
               console.log(`Due ${user.books[i].dueDate?.slice(0, 10)}\n`)
            }
         }
      } else {
         console.log("No books borrowed.")
      }
   }
}

export default listCommand;