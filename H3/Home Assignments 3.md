### H3.1 Mars Rover

**Deadline: 18.9.2020 10am**

Please save your home assignment as `H3_Firstname_Lastname.zip` and send it to the instructor as a DM in Discord OR if you'd like to, you can create a GitLab repository for your home assignments and send me a link to it. 

A Mars rover represented by a JS object moves in a flat surface, represented by a 10-by-10 grid. It can either move forward, turn 90 degrees left or turn 90 degrees right.

Write a program that includes the following features:

- the Mars rover as an object, containing at least 
    - parameters with default values `position: {x: 5, y: 0}` and `direction: "SOUTH"` 
    - methods `moveForward()`,  `turnLeft()` and `turnRight()` that update the position and direction of the rover and print a confirmation message to console
    - a `log` array that tracks the movement of the rover as it moves around in the grid
- some commands executed in succession, after which the current position and the whole log are printed with methods `printPosition()` and  `printLog()`


**Example:** When given the following commands:

    rover.moveForward();
    rover.turnRight();
    rover.moveForward();
    rover.turnLeft();
    rover.moveForward();
    rover.printPosition();
    rover.printLog();
    
the following messages should then be printed to the console:

    Moving towards SOUTH to coordinates 5,1.
    Turning right to face WEST.
    Moving towards WEST to coordinates 4,1.
    Turing left to face SOUTH.
    Moving towards SOUTH to coordinates 4,2.
    Currently positioned in 4,2.
    Travel Log:
        5,0
        5,1
        4,1
        4,2

### H4.2 Mars rover class

Create a Rover class with methods `moveForward()`,  `turnLeft()`, `turnRight()`, `printPosition()` and  `printLog()` and properties `position`, `direction` and `log`. Create the Mars rover as an instance of this class.

### H4.3 Two rovers

Create another instance of the Rover class. Give the rover a name and the starting position as an argument of the constructor function of the class.

**Extra**: Make it so that a rover can't move into coordinates where another other rover already is.

## Extra

### H4.4 2D map

Rewrite the `printLog()` function to draw the position as a 2D map:

If we use `x` to represent visited coordinates and `R` to represent rover's current position, at the end of the example of 4.1, `printLog()` should print

    - - - - - x - - - -
    - - - - x x - - - -
    - - - - R - - - - -
    - - - - - - - - - -
    - - - - - - - - - -
    - - - - - - - - - -
    - - - - - - - - - -
    - - - - - - - - - -
    - - - - - - - - - -
    - - - - - - - - - -

**Hint.** You can use e.g., a nested array to represent the 10-by-10 grid.

### H4.5 Collision detection

Make it impossible to go off limits. If the user tries to give orders to the rover that would result in going off the grid, print instead an error message.

### H4.6 Obstacles

Add obstacles to certain coordinates. The rover can't enter these coordinates; thus, trying so prompts an error message instead. Simultaneously the position of the obstacle gets saved in the log and after that it shows up as `O` in the `printLog()` command.

### H4.7 Command line interface

Make the Mars Rover program into a command line application, i.e., make it possible to give the commands `MOVE FORWARD`, `TURN LEFT`, `TURN RIGHT`, `PRINT POSITION` and `PRINT LOG` in the command line.