import IMarsRover from "./IMarsRover";

//MARS ROVER AS AN OBJECT
const rover: IMarsRover = {
    position: { x: 5, y: 0 },
    directions: ["NORTH", "EAST", "SOUTH", "WEST"],
    direction_i: 2,
    log: ['    5,0'],



    moveForward: () => {
        if (rover.direction_i % 4 === 0) {  //modulo 4 makes the array loop between 0 and 3
            rover.position.y--
            rover.log.push(`    ${rover.position.x},${rover.position.y}`);
        };
        if (rover.direction_i % 4 === 1) {
            rover.position.x++
            rover.log.push(`    ${rover.position.x},${rover.position.y}`);
        }
        if (rover.direction_i % 4 === 2) {
            rover.position.y++
            rover.log.push(`    ${rover.position.x},${rover.position.y}`);
        }
        if (rover.direction_i % 4 === 3) {
            rover.position.x--
            rover.log.push(`    ${rover.position.x},${rover.position.y}`);
        }
        console.log("Moving towards ", rover.directions[rover.direction_i % 4],
            "to coordinates ", rover.log[rover.log.length-1]);
    },

    turnLeft: () => {
        rover.direction_i--
        console.log("Turning left to face", rover.directions[rover.direction_i % 4]);
    },

    turnRight: () => {
        rover.direction_i++
        console.log("Turning right to face", rover.directions[rover.direction_i % 4]);
    },

    printPosition: () => {
        console.log("Currently positioned in", rover.log[rover.log.length-1]);
    },

    printLog: () => {
        console.log("Travel Log:")
        for (let i = 0; i < rover.log.length; i++) {
            console.log(rover.log[i]);
        }
    },


};





rover.moveForward();
rover.turnRight();
rover.moveForward();
rover.turnLeft();
rover.moveForward();
rover.printPosition();
rover.printLog();


