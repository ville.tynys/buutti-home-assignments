// ### H1.3
// Write a program that calculates a square root of `number`, and if `number` is negative, print out an error message.

// For example:
//  ```
//  node squareroot.js
//  Input number: 5
//  Square root for number 5 is 2.2360679775.

//  node squareroot.js
//  Input number: -5
//  Error: Number is negative.
//  ```


const readline = require('readline-sync');
let number = Number(readline.question('Give me a number plz : ')); //to make better we could test that user really inputs a number

if (number < 0) {
    console.log('Number is negative')
} else {
    number = Math.sqrt(number);
    console.log(number)
};