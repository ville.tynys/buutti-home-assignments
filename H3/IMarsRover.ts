export default interface IMarsRover{
    position: {x:number, y:number},
    directions: string [],
    direction_i: number
    log: string [],

    moveForward: () => void,
    turnLeft: () => void,
    turnRight: () => void,
    printPosition: () => void,
    printLog: () => void
    
};