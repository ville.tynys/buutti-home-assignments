/**
## H2.2 Deck of cards
Create a new array of 52 string elements.

### a) 
Populate the array with cards A, 2, 3, ..., 9, 10, J, Q, K with a suit of Heart, Club, Diamond and Spade.
So for instance `"Spade A"`, `"Club Q"`, `"Diamond 2"` and so on!

### b) 
Shuffle the deck.

### c) 
Create a function for taking the top-most card from the deck (the first in the array)
 * 
 */




//RANDOM SHUFFLE WITH FISHER YATES ALGORITHM
const fisherYatesShuffler = (a: any) => {
    let j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}




//FUNCTION FOR TAKING THE TOP CARD
const pickFromTop = (deck: string[]) => {
    let card = deck[0];
    deck.splice(0, 1); //remove the card from deck
    return card;
};




//POPULATING THE DECK
const suits = ['Heart', 'Club', 'Diamond', 'Spade'];
const values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
let deck = [];

for (let i = 0; i < suits.length; i++) {
    for (let j = 0; j < values.length; j++) {
        deck.push(suits[i] + ' ' + values[j]);
    }
};







console.log('Here is the shuffeled deck: ', fisherYatesShuffler(deck));
// console.log(deck.toString().match(/A/g)?.length); //check the number of A's

console.log('And the card picked from the top is: ', pickFromTop(deck));
console.log('And the deck afterwards: ', deck);



