class Rover {

    //parameters
    position: number[];     //x and y
    directions: string[] = ["NORTH", "EAST", "SOUTH", "WEST"];
    direction_i: number;
    grid_boundaries: any;
    log: string[] = [];

    //constructor
    constructor(grid_boundaries: any,
        position: number[] = [5, 0],
        direction_i = 2,
    )
    {
        this.position = position;
        this.direction_i = direction_i; //suunnan state
        this.grid_boundaries = grid_boundaries;
        this.log.push(this.position.toString())
    }

    moveForward() {
        if (this.direction_i % 4 === 0) { //jos north, y pienenee
            if (this.position[1] - 1 < this.grid_boundaries.north) {    //collision test 
                console.log("Error: Northern limit reached")
                return;
            }
            this.position[1]-- 
        } else if (this.direction_i % 4 === 2) { //jos south, y kasvaa
            if (this.position[1] + 1 > this.grid_boundaries.south) {
                console.log("Error: Southern limit reached")
                return;
            }
            this.position[1]++
        };

        if (this.direction_i % 4 === 1) { //jos east, x kasvaa
            if (this.position[0] + 1 > this.grid_boundaries.east) {
                console.log("Error: Eastern limit reached")
                return;
            }
            this.position[0]++
        } else if (this.direction_i % 4 === 3) { //jos west, x pienenee
            if (this.position[0] - 1 < this.grid_boundaries.west) {
                console.log("Error: Western limit reached")
                return;
            }
            this.position[0]--
        };

        this.log.push(this.position.toString())
        console.log("Moving towards ", this.directions[this.direction_i % 4], "to coordinates ", this.position)

    };

    turnLeft() {
        this.direction_i--;
        console.log("Turning left to face", this.directions[this.direction_i % 4],)
    };

    turnRight() {
        this.direction_i++;
        console.log("Turning right to face", this.directions[this.direction_i % 4],)
    };

    printPosition() {
        console.log("Currently positioned in", this.position)
    };

    printLog() {
        console.log("Travel Log: \n ", this.log);
    };

};

export default Rover;