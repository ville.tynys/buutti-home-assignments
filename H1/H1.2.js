// ### H1.2
// Create a program that prompts the user for a number that is between 0 and 10. If the number is between 0 and 10, console.log it. If not, console.log an error message.

// For example:
// ```
// node numberBetween.js
// Input number between 0 and 10: 10
// 10

// node numberBetween.js
// Input number between 0 and 10: 11
// Error: Number is not between 0 and 10.
// ```


const readline = require('readline-sync');
let number = readline.question('give me a number between 0 and 10 plz: ');

try {
    if (number >= 0 && number <= 10) {
        console.log(number);
    };
} catch (err) {
    console.log('something went wrong');
}
console.error('Error: Number is not between 0 and 10');




