class State {
  isLogin: boolean;
  user: string | null;

  constructor(isLogin = false, user = null) {
    this.isLogin = isLogin;
    this.user = user;
  }

  setLogin(user: string): void {
    this.isLogin = true;
    this.user = user;
  }

  setLoginFalse() {
    this.isLogin = false;
    this.user = null;
  }

  getLoginStatus(): boolean {
    return this.isLogin;
  }

  getUser(): string | null {
    return this.user;
  }
}

const state = new State();

export default state;
